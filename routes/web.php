<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/user-index', function () {
    return view('admin.index');
})->middleware('adminLogin');
// Route Admin
Route::group(['namespace' => 'admin'], function () {
    //Route  User Admin
    Route::group(['namespace' => 'user'], function () {
        Route::get('dang-ki', 'UserController@getregister')->name('admin.user.register');
        Route::get('user/logout', 'UserController@logOut')->name('admin.user.logout');
        Route::get('user/add-admin', 'UserController@add_admin')->name('admin.user.add_admin');
        Route::resource('user', 'UserController');
        
        
    });
});